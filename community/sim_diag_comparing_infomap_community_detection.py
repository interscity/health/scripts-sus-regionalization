#!/usr/bin/env python
# coding: utf-8

# # Applying Infomap [1, 2, 3] community detection technique on the network
# 
#  - Using ['infomap' python module](https://mapequation.github.io/infomap/python/)
# 
#  [1] Rosvall, M., Axelsson, D., and Bergstrom, C.T., 2009. The map equation. The European Physical
# Journal Special Topics, 178 (1), 13–23.  
#  [2] Rosvall, M. and Bergstrom, C.T., 2008. Maps of random walks on complex networks reveal community structure. Proceedings of the National Academy of Sciences of the United States of America,
# 105 (4), 1118–1123.  
#  [3] Rosvall, M. and Bergstrom, C.T., 2011. Multilevel compression of random walks on networks reveals
# hierarchical organization in large integrated systems. PLos One, 6 (4), e18209.

# In[1]:


import sys

# choosing the Brazilian state (e.g. 'AM-13-9', 'PA-15-12', 'BA-29-28', 'SP-35-63', 'RJ-33-9', 'RS-43-19')

print("Sigla do estado: ")
UF = 'RJ' #str(input())

estado_dict = {'RJ': ['33', 'Rio de Janeiro', '(-43.5,-22.9)', '(-43.7,-23.3)', '9', 'Rio de Janeiro'], 
               'RS': ['43', 'Porto Alegre', '(-51.15,-30.1)', '(-51,-32)', '19', 'Rio Grande do Sul'],
               'SP': ['35', 'São Paulo', '(-46.7,-23.5)', '(-46.1, -24.5)', '63', 'São Paulo'],
               'CE': ['23', 'Fortaleza', '(-38.57, -3.74)', '(-37.53, -3.9)', '22', 'Ceará'],
               'BA': ['29', 'Salvador', '(-38.4,-13)', '(-38.5,-14)', '28', 'Bahia']}
uf_code = int(estado_dict[UF][0])
capital = estado_dict[UF][1]
xy = eval(estado_dict[UF][2])
xytext = eval(estado_dict[UF][3])
regions_num = eval(estado_dict[UF][4])
uf_name = estado_dict[UF][5]


# In[2]:


regions_num = 3 #MACRO


# In[3]:


import pandas as pd
import geopandas as gpd
import numpy as np

# import the municipalities shapes
#mun_df = gpd.read_file("../data/%s/%s_mun.shp"%(UF, UF))
mun_df = gpd.read_file("../data/%s/%s_Municipios_%d.dbf"%(UF, UF, 2021))
mun_df['code'] = np.array([item[:-1] for item in np.array(mun_df.CD_MUN)]).astype('int') # trim last char
mun_df['centroid_x'] = mun_df.geometry.centroid.x
mun_df['centroid_y'] = mun_df.geometry.centroid.y
mun_df = mun_df.rename(columns={'NM_MUN': 'name'})

# # import the patient flow data
# df = pd.read_csv("../data/%s/%s_sih_flow.csv"%(UF, UF))

# import patient flow data for diff diagnosis
diag = sys.argv[2]+"_"
df = pd.read_csv("../data/%s/diagnosis/%ssih_flow.csv"%(UF, diag))


# In[30]:


mun_df


# **Removing incoming/outcomming hospitalizations out from the choosen state**

# In[31]:


df = df[df['mun_res']>uf_code*10000][df['mun_res']<(uf_code+1)*10000]
df = df[df['mun_cnes']>uf_code*10000][df['mun_cnes']<(uf_code+1)*10000]


# **Create dictionaries to favor the network creation process**

# In[32]:


# fetch municipalities name and positions
mun_dict = {'name': {}, 'position': {}, 'color': {}}

mun_dict['name'] = {int(item[0]):item[1] for item in mun_df[['code', 'name']].values}
mun_dict['position'] = {int(item[0]):(item[1], item[2]) for item in mun_df[['code', 'centroid_x', 'centroid_y']].values}


# **Config Infomap method**

# In[33]:


import infomap

# Partition network with the Infomap algorithm
def find_communities(G, v=False):

    # limit number of modules to X (same number of health reagions)
    im = infomap.Infomap("--silent --preferred-number-of-modules %d"%regions_num)

    if v: print("Building Infomap network from a NetworkX graph...")
    for source, target, D in G.edges(data=True):
        if D:
            im.add_link(source, target, weight=D['nhospadm_total'])
        else:
            im.add_link(source, target)    
            
    for node in G.nodes:
        im.add_node(node)

    if v: print("Find communities with Infomap...")
    im.run()

    if v: print(f"Found {im.num_top_modules} modules with codelength: {im.codelength}")

    communities = im.get_modules()
    nx.set_node_attributes(G, communities, 'community')
    
    return im
        


# **Config plots methods**

# In[34]:


import numpy as np

import networkx as nx

import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.lines import Line2D

def draw_communities(im, title_initial, title_final, year, save=False):
    color_map = mun_df.shape[0] * ['aqua']
    for node in im.tree:
        if node.is_leaf:
            if first: color = mun_dict['color'][node.node_id]
            color_map[np.where(mun_df['code'].values == node.node_id)[0][0]] = color
            first = 0
        else:
            first = 1

    fig, ax = plt.subplots(figsize=(20, 15))
    fig.set_facecolor('white')
#MACRO    gpd.read_file("../data/%s/regions/regions_%d.shp"%(UF, year)).plot(ax=ax, facecolor='none', edgecolor='k', zorder=2, lw=5)
    gpd.GeoSeries(mun_df['geometry'].values).plot(ax=ax, color=color_map, edgecolor='white', zorder=1)

    ax.annotate(capital, xy=xy, xytext=xytext, 
                arrowprops=dict(arrowstyle='->', lw=3), fontsize=30)
    
    regions_line = [Line2D([0, 1], [0, 2], color='k', alpha=0.8, lw=5)]
    gray_circle = [Line2D(range(1), range(1), color='w', marker='o', markerfacecolor='aqua', ms=15, mec='gray')]

#     plt.legend(regions_line+gray_circle, ['Limits of Health Regions', 'Municipalities without flow data'], loc=3, fontsize = 'x-large')
    plt.legend(regions_line, ['Limites das regiões de saúde'], bbox_to_anchor=(1, 0.02), fontsize = '42')
    
#     plt.suptitle(title_initial, fontsize=32)
#     plt.title('Detected '+str(im.num_top_modules)+' communities using \nInfomap network clustering algorithm'+title_final, fontsize = 'xx-large')
    plt.title('Comunidades detectadas pelo '+r"$\bf{infomap}$"+title_initial+'('+str(im.num_top_modules)+ ' comunidades)', fontsize = '52')
    plt.axis('off')
    if save: 
        plt.savefig("/home/gabriely/Projetos/qualificacao/figuras/infomap_%s.png"%UF.lower(), bbox_inches='tight')
#         plt.savefig("../data/%s/images/infomap/%dto%d.png"%(UF, init_year, init_year+4))
    else: return plt, ax
    
def draw_nodes(G, clrs):
    communities = [c - 1 for c in nx.get_node_attributes(G, 'community').values()]
    num_communities = max(communities) + 1

    cmap_light = colors.ListedColormap(
        clrs, 'indexed', num_communities)
        
    plt.figure(figsize=(16, 12))

    # nodes
    node_collection = nx.draw_networkx_nodes(
        G, pos=mun_dict['position'], node_color=communities, cmap=cmap_light)

    plt.axis('off')
    plt.show()


# **Choosing the palette in order to keep the same color to the same community over time**

# In[35]:


# get the "top nodes" (most significant node) for each community
def get_top_nodes(im):
    top_nodes = []
    first = 1
    for node in im.tree:
        if node.node_id != 0 and first == 1:
            top_nodes.append(node.node_id)
            first = 0
        elif node.node_id == 0:
            first = 1
    
    return top_nodes    


# In[36]:


# top_nodes = []

# for init_year in range(1999, 2020):

# #     flows = df[df['year']>=init_year][df['year']<init_year+5]
#     flows = df[df['year']==init_year]

#     # Summing the hospitalizations for all years
#     flows = flows[['mun_cnes', 'mun_res', 'nhospadm_total']].groupby(
#         by = ['mun_cnes', 'mun_res'], as_index = False).sum()

#     digraph = nx.from_pandas_edgelist(flows, source = 'mun_res', target = 'mun_cnes',
#                             edge_attr = 'nhospadm_total',create_using = nx.DiGraph())

#     im = find_communities(digraph, v=False)

#     top_nodes += get_top_nodes(im)


# In[37]:


# from palettable.colorbrewer.qualitative import Set3_12, Set1_9, Dark2_8

# top_nodes = np.unique(np.array(top_nodes))

# # palette = []
# # for a, b, c in [(2,63,165),(125,135,185),(190,193,212),(214,188,192),(187,119,132),(142,6,59),(74,111,227),(133,149,225),(181,187,227),(230,175,185),(224,123,145),(211,63,106),(17,198,56),(141,213,147),(198,222,199),(234,211,198),(240,185,141),(239,151,8),(15,207,192),(156,222,214),(213,234,231),(243,225,235),(246,196,225),(247,156,212)]:
# #     palette.append('#%02x%02x%02x'%(a, b, c))
# palette = Set3_12.hex_colors + Set1_9.hex_colors + Dark2_8.hex_colors
# mun_dict['color'] = {top_nodes[i] : palette[2*i%len(palette)] for i in range(len(top_nodes))}


# In[38]:


# np.unique(np.array(top_nodes))


# In[39]:


print("Ano: ")
init_year = int(sys.argv[1]) #2015
print(init_year)
# init_year = int(input()) #2015

# flows = df[df['year']>=init_year][df['year']<init_year+5]
flows = df[df['year']==init_year]

# Summing the hospitalizations for all years
flows


# In[40]:


flows = flows[['mun_cnes', 'mun_res', 'nhospadm_total']].groupby(
    by = ['mun_cnes', 'mun_res'], as_index = False).sum()


# In[41]:


digraph = nx.from_pandas_edgelist(flows, source = 'mun_res', target = 'mun_cnes',
                        edge_attr = 'nhospadm_total',create_using = nx.DiGraph())

im = find_communities(digraph)


# # Implementing metrics to compare network partitions
# 
#  - Using ['scikit learn' python module](https://scikit-learn.org/stable/index.html)

# In[42]:


from sklearn.metrics.cluster import adjusted_rand_score
from sklearn.metrics.cluster import v_measure_score #, completeness_score, homogeneity_score
# from sklearn.metrics import f1_score # F-measure

# from sklearn.metrics import jaccard_score
from sklearn.metrics.cluster import adjusted_mutual_info_score
from sklearn.metrics.cluster import fowlkes_mallows_score


# In[43]:


##FOR REGIONS
#year = 2011
#if init_year >= 2011: year = init_year
#if init_year > 2017: year = 2017

## import regions and municipalities relation
#reg_mun_df = pd.read_csv('../data/%s/regions/%s_regions_table_%d.csv'%(UF, UF, year)).sort_values(by='CD_MUN')
#
## get regions and municipalities relation arrays
#R = {mun[0]:mun[1] for mun in reg_mun_df.values}


# In[44]:


#FOR MACROREGIONS
# import regions and municipalities relation
year = 2020
reg_mun_df = pd.read_csv('../data/%s/regions/macroregions_latlong_%d.csv'%(UF, 2020)).sort_values(by='CD_MUN')

# get regions and municipalities relation arrays
R = {mun[0]:mun[1] for mun in reg_mun_df.values}


# In[45]:


none_flow_mun = reg_mun_df[reg_mun_df['CD_MUN'].isin(flows.sort_values(by='mun_res')['mun_res'].unique())==False]['CD_MUN']
for code in none_flow_mun:
    R.pop(code, None)


# In[46]:


print("Escolha o método (entre 1 e 4): ")
x = 2 #int(input())

if x == 1:
    index = adjusted_rand_score(list(R.values()), list(im.get_modules().values()))*100
    index_name = 'Adjusted Rand Index'
elif x == 2:
    index = v_measure_score(list(R.values()), list(im.get_modules().values()))*100
    index_name = 'V-Measure'
elif x == 3:
    index = adjusted_mutual_info_score(list(R.values()), list(im.get_modules().values()))*100
    index_name = 'Adjusted Mutual Info Score'
elif x == 4:
    index = fowlkes_mallows_score(list(R.values()), list(im.get_modules().values()))*100
    index_name = 'Fowlkes Mallows Score'  
else:
    index_name = 'Invalid option!'

print(index_name)
index_df = pd.read_csv("../../dashboard-regionalizacao-sus/data/comm_similarity_macro.csv", index_col='year')
index_df.loc[init_year, diag[:-1]] = index
print(index_df)
index_df.to_csv("../../dashboard-regionalizacao-sus/data/comm_similarity_macro.csv")


# # Comparing partitions Health Regions and Infomap detected commnities

# In[47]:


# from matplotlib.offsetbox import AnchoredText

# # title_initial = '\npara os dados entre %d e %d\n'%(init_year, init_year+4)
# title_initial = '\npara os dados de %d\n'%(init_year)
# plt, ax = draw_communities(im, title_initial, '', year)

# at = AnchoredText(
#     "%.1f%% de semelhança entre \nas regiões de saúde e \nas comunidades*"%index, 
#     prop=dict(size=36), frameon=True, loc='lower left')
# at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
# ax.add_artist(at)
# note = AnchoredText(
#     "                           * utilizando a métrica de comparação de partições '%s'"%index_name, 
#     prop=dict(size=20), frameon=False, loc='lower left', bbox_to_anchor=(0, 0))
# ax.add_artist(note)
# # plt.savefig("/home/gabriely/Mestrado/visualizing-health-regionalization-brazil/community/images/%s/%d.png"%(index_name, init_year), bbox_inches='tight')


# ----------------

# In[48]:


infomap_result = pd.DataFrame(im.modules, columns=['mun_code', 'community'])


# In[49]:


mun_df = mun_df.merge(infomap_result, how = 'left', left_on = ['code'], right_on = ['mun_code'])


# In[50]:


# from shapely.ops import cascaded_union
# from shapely.geometry import Polygon, MultiPolygon
# communities_boundary = []

# for community in mun_df[mun_df['community'].notna()]['community'].unique():
#     mun_polygons = mun_df[mun_df['community']==community]['geometry'].values
#     community_polygon = cascaded_union(mun_polygons)
    
    
#     if community_polygon.type == 'MultiPolygon':
#         community_polygon = list(community_polygon)
#         multipoly = []
#         for _ in range(2):
#             maxx = max(community_polygon, key=lambda a: a.area)
#             poly = Polygon(maxx.exterior)
#             multipoly.append(poly)
#             community_polygon.remove(maxx)
#         communities_boundary.append(MultiPolygon(multipoly))
#     else:
#         poly = Polygon(community_polygon.exterior)
#         communities_boundary.append(poly)


# In[51]:


# import matplotlib.pyplot as plt

# # plot the health regions shape
# fig, ax = plt.subplots(figsize=(12, 10))
# #gpd.GeoSeries(mun_df['geometry']).plot(ax=ax, color="orange", edgecolor="darkcyan", lw=1)
# gpd.GeoSeries(communities_boundary).plot(ax=ax, color="orange", alpha=0.5, edgecolor="k", lw=2)
# ax.axis("off")
# plt.title("Detected communities in the State of %s (%d regions)"%(UF, len(communities_boundary)))


# In[53]:


# macro = ''
# # macro = 'macro_'

# # saving communities shape to file

# gdf = gpd.GeoDataFrame({
#     'community': mun_df[mun_df['community'].notna()]['community'].unique(),
#     'geometry': communities_boundary
# })

# gdf['geometry'] = gdf['geometry'].simplify(tolerance=0.01)
# # gdf.to_file("../data/%s/communities/%scomm_%d.shp"%(UF, macro, init_year))
# # gdf.to_file("../../dashboard-regionalizacao-sus/data/shapes/communities/%scomm_%d.shp"%(macro, init_year))
# # For diagnosis
# gdf.to_file("../data/%s/communities/diagnosis/%scomm_%d.shp"%(UF, macro, init_year))
# gdf.to_file("../../dashboard-regionalizacao-sus/data/shapes/communities/diagnosis/%s%scomm_%d.shp"%(diag, macro, init_year))


# -----------

# In[ ]:


# import pandas as pd

# data = {'init year': list(range(2000, 2016)),
#         'Adjusted Rand Index':  [0]*16,
#         'V-Measure': [0]*16,
#         'Adjusted Mutual Info Score': [0]*16,
#         'Fowlkes Mallows Score': [0]*16
#         }

# df = pd.DataFrame(data)


# In[ ]:


# df.loc[init_year-2000, index_name] = index


# In[ ]:


# df.to_csv(index=False)


# In[351]:


# from bokeh.plotting import figure, show, output_notebook
# # from bokeh.models import HoverTool
# UF = 'CE'
# df = pd.read_csv('comparing_%s.csv'%UF)

# # prepare some data
# x = df['init year'].to_list()
# y1 = df['Adjusted Rand Index'].to_list()
# y2 = df['V-Measure'].to_list()
# y3 = df['Adjusted Mutual Info Score'].to_list()
# y4 = df['Fowlkes Mallows Score'].to_list()

# # create a new plot with a title and axis labels
# p = figure(title="Algoritmos de comparação de partições - %s"%uf_name, x_axis_label="Ano de início", 
#            y_axis_label="Nível de semelhança (%)", y_range=(0, 100))
# # p.add_tools(HoverTool())

# # add multiple renderers
# # p.scatter(x, y1, marker="square", fill_color="blue")
# p.line(x, y2, legend_label="V-Measure", line_color="red", line_width=2)
# p.line(x, y3, legend_label="Adjusted Mutual Info Score", line_color="green", line_width=2)
# p.line(x, y4, legend_label="Fowlkes Mallows Score", line_color="yellow", line_width=2)
# p.line(x, y1, legend_label="Adjusted Rand Index", line_color="blue", line_width=2)

# # show the results
# output_notebook(hide_banner=True)
# show(p)


# In[352]:


# from bokeh.plotting import figure, show, output_notebook
# import pandas as pd
# # from bokeh.models import HoverTool
# UF = 'CE'
# df = pd.read_csv('comparing_%s_single_year.csv'%UF)

# # prepare some data
# x = df['year'].to_list()
# y1 = df['Adjusted Rand Index'].to_list()
# y2 = df['V-Measure'].to_list()
# y3 = df['Adjusted Mutual Info Score'].to_list()
# y4 = df['Fowlkes Mallows Score'].to_list()

# # create a new plot with a title and axis labels
# p = figure(title="Algoritmos de comparação de partições - %s"%uf_name, 
#            x_axis_label="Ano", y_axis_label="Nível de semelhança (%)", y_range=(0, 100))
# # p.add_tools(HoverTool())

# # add multiple renderers
# # p.scatter(x, y1, marker="square", fill_color="blue")
# p.line(x, y2, legend_label="V-Measure", line_color="red", line_width=2)
# p.line(x, y3, legend_label="Adjusted Mutual Info Score", line_color="green", line_width=2)
# p.line(x, y4, legend_label="Fowlkes Mallows Score", line_color="yellow", line_width=2)
# p.line(x, y1, legend_label="Adjusted Rand Index", line_color="blue", line_width=2)
# p.legend.location = "bottom_right"

# # show the results
# output_notebook(hide_banner=True)
# show(p)

