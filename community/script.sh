#!/bin/bash

for ano in 1999 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 2016 2017 2018 2019
do
   for diag in skin_diseases mental_disorders eye_ear_diseases musculoskeletal_system parasitic_diseases diabetes external_causes genitourinary_system nervous_system neoplasm circulatory_system pregnancy
   do
   	python3 sim_diag_comparing_infomap_community_detection.py $ano $diag
   done
done
