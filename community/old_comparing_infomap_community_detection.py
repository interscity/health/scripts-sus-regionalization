#!/usr/bin/env python
# coding: utf-8

# # Applying Infomap [1, 2, 3] community detection technique on the network
# 
#  - Using ['infomap' python module](https://mapequation.github.io/infomap/python/)
# 
#  [1] Rosvall, M., Axelsson, D., and Bergstrom, C.T., 2009. The map equation. The European Physical
# Journal Special Topics, 178 (1), 13–23.  
#  [2] Rosvall, M. and Bergstrom, C.T., 2008. Maps of random walks on complex networks reveal community structure. Proceedings of the National Academy of Sciences of the United States of America,
# 105 (4), 1118–1123.  
#  [3] Rosvall, M. and Bergstrom, C.T., 2011. Multilevel compression of random walks on networks reveals
# hierarchical organization in large integrated systems. PLos One, 6 (4), e18209.

# In[62]:



# choosing the Brazilian state (e.g. 'AM-13-9', 'PA-15-12', 'BA-29-28', 'SP-35-63', 'RJ-33-9', 'RS-43-19')

UF = 'CE'

estado_dict = {'RJ': ['33', 'Rio de Janeiro', '(-43.5,-22.9)', '(-43.7,-23.3)', '9'], 
               'RS': ['43', 'Porto Alegre', '(-51.15,-30.1)', '(-51,-32)', '19'],
               'SP': ['35', 'São Paulo', '(-46.7,-23.5)', '(-46.1, -24.5)', '63', 'São Paulo'],
               'CE': ['23', 'Fortaleza', '(-38.57, -3.74)', '(-37.53, -3.9)', '22', 'Ceará'],
               'BA': ['29', 'Salvador', '(-38.4,-13)', '(-38.5,-14)', '28']}
uf_code = int(estado_dict[UF][0])
capital = estado_dict[UF][1]
xy = eval(estado_dict[UF][2])
xytext = eval(estado_dict[UF][3])
regions_num = eval(estado_dict[UF][4])


# In[41]:


import pandas as pd
import geopandas as gpd

# import the municipalities shapes
mun_df = gpd.read_file("../data/%s/%s_mun.shp"%(UF, UF))

# import the patient flow data
df = pd.read_csv("../data/%s/%s_sih_flow.csv"%(UF, UF))

df_compare = pd.DataFrame({
        'year': list(range(2002, 2020)),
        'Adjusted Rand Index':  [0]*16,
        'V-Measure': [0]*16,
        'Adjusted Mutual Info Score': [0]*16,
        'Fowlkes Mallows Score': [0]*16
        })



# **Removing incoming/outcomming hospitalizations out from the choosen state**

# In[42]:


df = df[df['mun_res']>uf_code*10000][df['mun_res']<(uf_code+1)*10000]
df = df[df['mun_cnes']>uf_code*10000][df['mun_cnes']<(uf_code+1)*10000]


# **Create dictionaries to favor the network creation process**

# In[43]:


# fetch municipalities name and positions
mun_dict = {'name': {}, 'position': {}, 'color': {}}

mun_dict['name'] = {int(item[0]):item[1] for item in mun_df[['code', 'name']].values}
mun_dict['position'] = {int(item[0]):(item[1], item[2]) for item in mun_df[['code', 'centroid_x', 'centroid_y']].values}


# **Config Infomap method**

# In[44]:


import infomap

# Partition network with the Infomap algorithm
def find_communities(G, v=False):

    # limit number of modules to X (same number of health reagions)
    im = infomap.Infomap("--silent --preferred-number-of-modules %d"%regions_num)

    if v: print("Building Infomap network from a NetworkX graph...")
    for source, target, D in G.edges(data=True):
        if D:
            im.add_link(source, target, weight=D['nhospadm_total'])
        else:
            im.add_link(source, target)    
            
    for node in G.nodes:
        im.add_node(node)

    if v: print("Find communities with Infomap...")
    im.run()

    if v: print(f"Found {im.num_top_modules} modules with codelength: {im.codelength}")

    communities = im.get_modules()
    nx.set_node_attributes(G, communities, 'community')
    
    return im
        


# **Config plots methods**

# In[45]:


import numpy as np

import networkx as nx

import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.lines import Line2D

def draw_communities(im, title_initial, title_final, save=False):
    color_map = mun_df.shape[0] * ['aqua']
    for node in im.tree:
        if node.is_leaf:
            if first: color = mun_dict['color'][node.node_id]
            color_map[np.where(mun_df['code'].values == node.node_id)[0][0]] = color
            first = 0
        else:
            first = 1

    fig, ax = plt.subplots(figsize=(20, 15))
    fig.set_facecolor('white')
    gpd.read_file("../data/%s/regions/regions_2017.shp"%(UF)).plot(ax=ax, facecolor='none', edgecolor='k', zorder=2, lw=5)
    gpd.GeoSeries(mun_df['geometry'].values).plot(ax=ax, color=color_map, edgecolor='white', zorder=1)

    ax.annotate(capital, xy=xy, xytext=xytext, 
                arrowprops=dict(arrowstyle='->', lw=3), fontsize=30)
    
    regions_line = [Line2D([0, 1], [0, 2], color='k', alpha=0.8, lw=5)]
    gray_circle = [Line2D(range(1), range(1), color='w', marker='o', markerfacecolor='aqua', ms=15, mec='gray')]

#     plt.legend(regions_line+gray_circle, ['Limits of Health Regions', 'Municipalities without flow data'], loc=3, fontsize = 'x-large')
    plt.legend(regions_line, ['Limites das regiões de saúde'], bbox_to_anchor=(1, 0.02), fontsize = '42')
    
#     plt.suptitle(title_initial, fontsize=32)
#     plt.title('Detected '+str(im.num_top_modules)+' communities using \nInfomap network clustering algorithm'+title_final, fontsize = 'xx-large')
    plt.title('Comunidades detectadas pelo '+r"$\bf{infomap}$"+title_initial+'('+str(im.num_top_modules)+ ' comunidades)', fontsize = '52')
    plt.axis('off')
    if save: 
        plt.savefig("/home/gabriely/Projetos/qualificacao/figuras/infomap_%s.png"%UF.lower(), bbox_inches='tight')
#         plt.savefig("../data/%s/images/infomap/%dto%d.png"%(UF, init_year, init_year+4))
    else: return plt, ax
    
def draw_nodes(G, clrs):
    communities = [c - 1 for c in nx.get_node_attributes(G, 'community').values()]
    num_communities = max(communities) + 1

    cmap_light = colors.ListedColormap(
        clrs, 'indexed', num_communities)
        
    plt.figure(figsize=(16, 12))

    # nodes
    node_collection = nx.draw_networkx_nodes(
        G, pos=mun_dict['position'], node_color=communities, cmap=cmap_light)

    plt.axis('off')
    plt.show()


# **Choosing the palette in order to keep the same color to the same community over time**

# In[46]:


# get the "top nodes" (most significant node) for each community
def get_top_nodes(im):
    top_nodes = []
    first = 1
    for node in im.tree:
        if node.node_id != 0 and first == 1:
            top_nodes.append(node.node_id)
            first = 0
        elif node.node_id == 0:
            first = 1
    
    return top_nodes    


# In[47]:


top_nodes = []

for init_year in range(1994, 2016):

	print (">>> ", init_year)
	flows = df[df['year']>=init_year][df['year']<init_year+5]

	# Summing the hospitalizations for all years
	flows = flows[['uf_code', 'mun_cnes', 'mun_res', 'nhospadm_total']].groupby(
		by = ['uf_code', 'mun_cnes', 'mun_res'], as_index = False).sum()

	digraph = nx.from_pandas_edgelist(flows, source = 'mun_res', target = 'mun_cnes',
		                    edge_attr = 'nhospadm_total',create_using = nx.DiGraph())

	im = find_communities(digraph, v=False)

	top_nodes += get_top_nodes(im)


	# In[48]:


from palettable.colorbrewer.qualitative import Set3_12, Set1_9, Dark2_8

top_nodes = np.unique(np.array(top_nodes))

palette = Set3_12.hex_colors + Set1_9.hex_colors + Dark2_8.hex_colors
mun_dict['color'] = {top_nodes[i] : palette[i%len(palette)] for i in range(len(top_nodes))}


# **Group and sum flow data every 5 years (from 1994-2019)**

# In[49]:
#######################################3
for init_year in range(2000, 2016):
	print("Ano de início: ", init_year)

	flows = df[df['year']>=init_year][df['year']<init_year+5]

	# Summing the hospitalizations for all years
	flows = flows[['uf_code', 'mun_cnes', 'mun_res', 'nhospadm_total']].groupby(
		by = ['uf_code', 'mun_cnes', 'mun_res'], as_index = False).sum()


	# In[50]:


	digraph = nx.from_pandas_edgelist(flows, source = 'mun_res', target = 'mun_cnes',
			            edge_attr = 'nhospadm_total',create_using = nx.DiGraph())

	im = find_communities(digraph)


	# # Implementing metrics to compare network partitions
	# 
	#  - Using ['scikit learn' python module](https://scikit-learn.org/stable/index.html)

	# In[51]:


	from sklearn.metrics.cluster import adjusted_rand_score
	from sklearn.metrics.cluster import v_measure_score #, completeness_score, homogeneity_score
	# from sklearn.metrics import f1_score # F-measure

	# from sklearn.metrics import jaccard_score
	from sklearn.metrics.cluster import adjusted_mutual_info_score
	from sklearn.metrics.cluster import fowlkes_mallows_score


	# In[52]:

	# como utilizo 5 anos agregados de dados para o algorítimo, comparo com a partição de reegioões de saúde do ano do meio desse agregado (o 3o ano). Lembrando que, por enquanto, tenho as regiões de 2011 a 2017.
	year = 2011
	if init_year >= 2010: year = init_year+2

	# import regions and municipalities relation
	reg_mun_df = pd.read_csv('../data/%s/regions/%s_regions_table_%d.csv'%(UF, UF, year)).sort_values(by='CD_MUN')

	# get regions and municipalities relation arrays
	R = {mun[0]:mun[1] for mun in reg_mun_df.values}


	# In[58]:


	for x in range(1, 5):
		if x == 1:
			index = adjusted_rand_score(list(R.values()), list(im.get_modules().values()))*100
			index_name = 'Adjusted Rand Index'
		elif x == 2:
			index = v_measure_score(list(R.values()), list(im.get_modules().values()))*100
			index_name = 'V-Measure'
		elif x == 3:
			index = adjusted_mutual_info_score(list(R.values()), list(im.get_modules().values()))*100
			index_name = 'Adjusted Mutual Info Score'
		elif x == 4:
			index = fowlkes_mallows_score(list(R.values()), list(im.get_modules().values()))*100
			index_name = 'Fowlkes Mallows Score'  
		else:
			index_name = 'Invalid option!'

		print(index_name)
		df_compare.loc[init_year-2000, index_name] = index


		# # Comparing partitions Health Regions and Infomap detected commnities

		# In[60]:


		from matplotlib.offsetbox import AnchoredText

		#title_initial = '\nutilizando todos os %d fluxos\n'%(flows.shape[0])
		#title_initial = '\npara os dados entre %d e %d\n'%(init_year, init_year+4)
		#plt, ax = draw_communities(im, title_initial, '')

		#at = AnchoredText(
		#	"%.1f%% de semelhança entre \nas regiões de saúde e \nas comunidades*"%index, 
		#	prop=dict(size=36), frameon=True, loc='lower left')
		#at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
		#ax.add_artist(at)
		#note = AnchoredText(
		#	"                           * utilizando a métrica de comparação de partições '%s'"%index_name, 
		#	prop=dict(size=20), frameon=False, loc='lower left', bbox_to_anchor=(0, 0))
		#ax.add_artist(note)
		#plt.savefig("/home/gabriely/Mestrado/visualizing-health-regionalization-brazil/community/images/%s/%d.png"%(index_name, init_year), bbox_inches='tight')
		
df_compare.to_csv('comparing_%s_middle_reg.csv'%UF, index=False)


