#!/bin/bash

for diag in skin_diseases mental_disorders parasitic_diseases external_causes genitourinary_system pregnancy
do
   python3 create_communities_shapes.py CE macroregional $diag
done
