# FALTA ADICIONAR OPÇÃO DE RODAR TTTTUUUUUDDDDOOOOO DE UMA VEEEEEZZZZZ

'''
    Creates the CSV files for a state with the 
    municipality, region and macroregion flow data for each available year.
    It is also possible to filter by diagnosis.

    Columns: ['year', 'from', 'to', 'hospitalizations'],
        where 'from' and 'to' are dictionaries with keys: 'latlong', 'code' and 'name'.
    
    Run: python3 create_sih_flow.py <UF> <municipal|regional|macroregional> <diagnosis(optional)>

    Creates "*_sih_flow.csv" for a specific Brazilian state.
'''

import sys
import math
import pandas as pd
import geopandas as gpd
import numpy as np
import geopy.distance as gp

if(len(sys.argv) < 3): sys.exit("[error] run: python3 create_sih_flow.py <UF> <municipal|regional|macroregional> <diagnosis(optional)>")
if(sys.argv[2] != "municipal" and sys.argv[2] != "regional" and sys.argv[2] != "macroregional"): sys.exit("[error] Invalid value. Valid options: municipal regional macroregional")

# get state abbreviation (UF)
UF = sys.argv[1]

# import the municipalities shapes from the IBGE municipalities dataset (2021)
#   -> (https://www.ibge.gov.br/geociencias/organizacao-do-territorio/malhas-territoriais)
mun_df = gpd.read_file("../../data/%s/%s_Municipios_2021.dbf"%(UF, UF), encoding = "utf-8")
mun_df['code'] = np.array([item[:-1] for item in np.array(mun_df.CD_MUN)]).astype('int') # trim last char
mun_df['centroid_x'] = mun_df.geometry.centroid.x
mun_df['centroid_y'] = mun_df.geometry.centroid.y

if(sys.argv[2] != "municipal"):
    # import the municipality-region-macroregion relations
    reg_macro_df = pd.read_csv("../../data/%s/reg_macro_table.csv"%(UF))

    # import region and macroregion shapes
    regions_shape = gpd.read_file("../../data/%s/regions.shp"%UF)
    macroregions_shape = gpd.read_file("../../data/%s/macroregions.shp"%UF)

# read the patient flows data
if(UF == 'RJ'): original_df = pd.read_csv("../../../../SESSP/RJ_CID.csv", low_memory=False)
elif(UF == 'SP'): original_df = pd.read_csv("../../../../SP/%s_CID.csv"%UF, low_memory=False)
elif(UF == 'CE'): original_df = pd.read_csv("../../../../NESBA/NESBA_CID.csv", low_memory=False)

original_df = original_df.dropna(subset=['mun_res', 'mun_cnes'])
print(original_df)

original_df['nhospadm_total'] = 1

# remove all interstate flows
uf_code = mun_df['code'].values[0]//10000
original_df = original_df[original_df['mun_res']>uf_code*10000][original_df['mun_res']<(uf_code+1)*10000]
original_df = original_df[original_df['mun_cnes']>uf_code*10000][original_df['mun_cnes']<(uf_code+1)*10000]

# filter by a specific diagnosis
diagnosis_codes = {'skin_diseases': 'L','mental_disorders': 'F','eye_ear_diseases': 'H','musculoskeletal_system': 'M','parasitic_diseases': 'B','diabetes': 'E10|E11|E12|E13|E14','external_causes': 'S|T|V|W|X|Y','genitourinary_system': 'N','nervous_system': 'G','neoplasm': 'C|D0|D1|D2|D3|D4','circulatory_system': 'I','pregnancy': 'O|P|Z3'}


def get_name(reg, year, code):
    available_year = year
    if(year < 2011): available_year = 2011
    elif(year > 2017): available_year = 2017
    if(reg == 'macro'): available_year = 2020

    if(reg == 'reg'): reg = 'region'
    else: reg = 'macroregion'

    tmp = reg_macro_df[reg_macro_df['year'] == available_year]
    result = tmp[tmp['code'] == code][reg]
    if(result.empty or type(result.values[0])!=str): return None
    return result.values[0]

def get_latlong(reg, year, code):
    region_name = get_name(reg, year, code)

    available_year = year
    if(year < 2011): available_year = 2011
    elif(year > 2017): available_year = 2017
    if(reg == 'macro'): available_year = 2020

    if(reg == 'reg'): shape = regions_shape
    else: shape = macroregions_shape

    tmp = shape[shape['year'] == available_year]
    geometries = tmp[tmp['name'].isin([region_name])]['geometry']
    geometry = max(geometries, key=lambda a: a.area, default=None)
    
    if(geometry):
        if(geometry.type == 'MultiPolygon'): geometry = max(geometry, key=lambda a: a.area)
        lat = geometry.exterior.centroid.xy[0][0]
        lon = geometry.exterior.centroid.xy[1][0]
    else: lat, lon = None, None

    return [lat, lon]

def get_distance(x):
    coord1, coord2 = eval(x['from'])['latlong'], eval(x['to'])['latlong']
    return gp.distance((coord1[0], coord1[1]), (coord2[0], coord2[1])).km


for diag in list(diagnosis_codes.keys()):
    df = original_df.copy()

    if(len(sys.argv) > 3):
        #diagnosis = sys.argv[3]
        print('v----', diag, '----v')
        diagnosis = diag
        diag_code = diagnosis_codes[diagnosis]
        tmp = original_df['cid_pri10'].str.contains(diag_code, case=True, na=False, regex=True)
        df['nhospadm_total'] = tmp.astype('int')
        diagnosis += '_'
    else: diagnosis = ''

    df = df.groupby(
        by = ['ano_adm', 'mun_cnes', 'mun_res'], as_index = False).sum()
    df = df[df.nhospadm_total >= 1]
    df = df.rename(columns={"ano_adm": "year"})

    municipal_df = df[['year','mun_res','mun_cnes','nhospadm_total']]
    my_dict = {int(item[0]): {'latlong':[item[1], item[2]], 'code': int(item[0]), 'name':item[3]} for item in mun_df[['code', 'centroid_x', 'centroid_y', 'NM_MUN']].values}
    municipal_df['mun_res'] = [str(my_dict[code]) for code in municipal_df['mun_res']]
    municipal_df['mun_cnes'] = [str(my_dict[code]) for code in municipal_df['mun_cnes']]

    municipal_df = municipal_df.rename(columns={'mun_res':'from', 'mun_cnes':'to', 'nhospadm_total':'hospitalizations'})


    if(sys.argv[2] == "municipal"): 
        # calculate centroid distances
        municipal_df['distance'] = municipal_df.apply(lambda x: get_distance(x), axis=1)

        print(municipal_df)
        municipal_df.to_csv("../../data/%s/diag/mun_%ssih_flow.csv"%(UF, diagnosis), index=False)

    else:
        if(sys.argv[2] == "regional"): reg = 'reg'
        else: reg = 'macro'

        regional_df = municipal_df.copy()
        regional_df['from'] = regional_df.apply(lambda x: {'latlong':get_latlong(reg, x['year'], eval(x['from'])['code']), 
                                                        'name':get_name(reg, x['year'], eval(x['from'])['code'])}, axis=1)
        regional_df['to'] = regional_df.apply(lambda x: {'latlong':get_latlong(reg, x['year'], eval(x['to'])['code']), 
                                                        'name':get_name(reg, x['year'], eval(x['to'])['code'])}, axis=1)

        regional_df['from'] = regional_df['from'].map(str)
        regional_df['to'] = regional_df['to'].map(str)

        regional_df = regional_df.groupby(
            by = ['year', 'from', 'to'], as_index = False).sum()
        
        # calculate centroid distances
        regional_df['distance'] = regional_df.apply(lambda x: get_distance(x), axis=1)

        print(regional_df)
        regional_df.to_csv("../../data/%s/diag/%s_%ssih_flow.csv"%(UF, reg, diagnosis), index=False)

