'''
    Creates the CSV file for a state with the 
    municipality-region-macroregion relation for each available year.
    
    Columns: ['year', 'code', 'region', 'macroregion'],

    Run: python3 create_reg_macro_table.py <UF>

    Creates "reg_macro_table.csv" for a specific Brazilian state.
'''

import sys
import pandas as pd
import geopandas as gpd
import numpy as np

# get state abbreviation (UF)
UF = sys.argv[1]

# this file contains the health regions partitions from 2011 to 2017
regions = pd.read_csv("../../data/panel_mun_cir.csv", encoding = "ISO-8859-1")
regions = regions[regions['UF'] == UF]

# this file contains the health regions partition for 2019
regions_2019_df = pd.io.stata.read_stata('../../data/Regioes de Saude.dta')
regions_2019_df = regions_2019_df[regions_2019_df['uf'] == UF]

# this file contains the health macroregions partition for 2019
macroregions = pd.io.stata.read_stata('../../data/Macrorregioes de Saude.dta')
macroregions = macroregions[macroregions['uf'] == UF]

# import the municipalities shapes from the IBGE municipalities dataset (2021)
#   -> (https://www.ibge.gov.br/geociencias/organizacao-do-territorio/malhas-territoriais)
mun_df_original = gpd.read_file("../../data/%s/%s_Municipios_2021.dbf"%(UF, UF))
mun_df_original['CD_MUN'] = np.array([item[:-1] for item in np.array(mun_df_original['CD_MUN'])]).astype('int') # trim last char

final_df = pd.DataFrame({'code': [''],
                         'region': [''],
                         'macroregion': ['']}, index=[0]) 

init = 2011
if(UF == 'SP'): init = 2012
# REGIONS USUALY BETWEEN 2011 AND 2017
for year in range(init, 2017+1):
    mun_df = mun_df_original.copy().drop(columns=['NM_MUN', 'SIGLA', 'AREA_KM2', 'geometry'])
    mun_df['year'] = year
    
    upper_limit = regions['file_date'] < str(year+1) 
    lower_limit = regions['file_date'] > str(year)

    is_this_year = np.logical_and(upper_limit, lower_limit)
    regions_this_year = regions[is_this_year]

    last_update_date = np.unique(regions_this_year['file_date'])[-1]
    regions_df = regions_this_year[regions_this_year['file_date']==last_update_date]

    mun_df = mun_df.merge(regions_df[['CODMUN','CIRNOME']], 
                          how = 'left', left_on = ['CD_MUN'], right_on = ['CODMUN'])
    mun_df = mun_df.rename(columns={'CIRNOME': 'region', 'CD_MUN': 'code'})
    mun_df = mun_df.drop(columns=['CODMUN'])

    mun_df['macroregion'] = ''
    final_df = pd.concat([final_df, mun_df], axis=0, sort=False, ignore_index=True)

# REGIONS FOR 2019
mun_df = mun_df_original.copy().drop(columns=['NM_MUN', 'SIGLA', 'AREA_KM2', 'geometry'])
mun_df['year'] = 2019
mun_df = mun_df.merge(regions_2019_df[['ibge','no_regiao']], 
                      how = 'left', left_on = ['CD_MUN'], right_on = ['ibge'])
mun_df = mun_df.rename(columns={'no_regiao': 'region', 'CD_MUN': 'code'})
mun_df = mun_df.drop(columns=['ibge'])

mun_df['macroregion'] = ''

# MACROREGIONS ONLY AVAILABLE FOR 2019
mun_df = mun_df.merge(macroregions[['ibge','no_macro']], 
                      how = 'left', left_on = ['code'], right_on = ['ibge'])
mun_df = mun_df.drop(columns=['ibge', 'macroregion'])
mun_df = mun_df.rename(columns={'no_macro': 'macroregion'})

final_df = pd.concat([final_df, mun_df], axis=0, sort=False, ignore_index=True)

print(final_df[1:][['year', 'code', 'region', 'macroregion']])

final_df[1:][['year', 'code', 'region', 'macroregion']].to_csv("../../data/%s/reg_macro_table.csv"%(UF), index=False)
